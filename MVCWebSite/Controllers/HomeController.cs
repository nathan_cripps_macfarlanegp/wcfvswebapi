﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVCWebSite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult GetStockData()
        {
            var service = new StockServiceWCFReference.StockServiceClient();
            var stockDate = service.GetStockData();

            return Json(stockDate);
        }

        public async Task<JsonResult> GetStockDataCallingWebAPI()
        {
            var ServiceUrl = "http://localhost/StockServiceAPI/GetStockData"; //probably use a config value for this.
            var  _client = new HttpClient();

            var response = _client.GetAsync(ServiceUrl);
            return Json(response.Result);
        }
    }
}