﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace WCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class StockService : IStockService
    {
        public Dictionary<string, decimal> GetStockData()
        {
            var dictionary = new Dictionary<string, decimal>();
            dictionary.Add("Potato", 100.345345m);
            dictionary.Add("ACME", 45.257m);
            dictionary.Add("FoodRUs", 121.43m);
            dictionary.Add("MegaChain", 7.864m);
            dictionary.Add("PriceBusters", 27.32342343m);
            return dictionary;
        }
    }
}
