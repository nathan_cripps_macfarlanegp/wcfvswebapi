﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace WebApplication1.Controllers
{
    [Route("api/stock")]
    public class StockController : Controller
    {
        // GET: api/values
        [HttpGet]
        public Dictionary<string, decimal> GetStock()
        {
            var dictionary = new Dictionary<string, decimal>();
            dictionary.Add("Potato", 100.345345m);
            dictionary.Add("ACME", 45.257m);
            dictionary.Add("FoodRUs", 121.43m);
            dictionary.Add("MegaChain", 7.864m);
            dictionary.Add("PriceBusters", 27.32342343m);
            return dictionary;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
